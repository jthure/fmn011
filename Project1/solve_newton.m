function [ x,iterations ] = solve_newton( f, f_prime, x0 )
%SOLVE_NEWTON solves the equation f(x) = 0 with Newton's method
%   f is the function f(x) and f_prime is the derivative of f(x), df/dx. x0
%   is the initial guess. The algorithm will continue until a
%   minimal residual is achieved.

    x = x0 - f(x0) / f_prime(x0);
    iterations=1;
    while abs(f(x))>0;
        %e0=x-(-1.3325);
        x = x - f(x) / f_prime(x);
        %e1=x-(-1.3325);
        %error_convergence=abs(e1/(e0))
        iterations=iterations+1;
    end
    
end