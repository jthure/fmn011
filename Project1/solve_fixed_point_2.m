function [ x, iterations] = solve_fixed_point_2( g, x0)
%SOLVE_FIXED_POINT_2 Solves the equation x = f(x) using fixed point method
%   x0 is the initial guess and tolerance is the accuracy. The algorithm will continue until a
%   minimal residual is achieved.

    x = g(x0);
    iterations = 1;
    while abs(g(x)-x) > 0
        e0=g(x)-x;
        x=g(x);
        e1=g(x)-x;
        error_convergence=e1/e0
        iterations = iterations + 1;
    end
end