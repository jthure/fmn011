function [ result, possible_error, residual, iterations] = solve_bisection_2( f,a,b)
%SOLVE_BISECTION_2 Solves the function f(x) = 0 with bisection method.
%   a and b are starting points and are assumed to satisfy f(a)*f(b)<0. Furthermore
%   f is assumed to be continuous. The algorithm will continue until a
%   minimal residual is achieved.

    iterations = 1;
    c = (a+b)/2;
    while abs(f(c)) > 0 
        e0=(b-a)/2;
        c = (a+b)/2;
        if f(c)*f(a)>0
            a=c;
            iterations = iterations + 1; 
        elseif f(c)*f(b)>0
            b=c;
            iterations = iterations + 1;
        else break
        end
        e1=(b-a)/2;
        error_convergence=abs(e1/e0)
    end
    possible_error=(b-a)/2;
    residual=f(c);
    result = c;
end