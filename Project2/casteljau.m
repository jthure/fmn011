function [ curveX,curveY ] = casteljau( x,y,t)
%CASTELJAU Summary of this function goes here
%   Detailed explanation goes here

    if(length(x)~=length(y))
        error('Vectors x and y must be of same length');
    end
    
    n=length(x);

    for i=1:n
        for j=1:n-i
            x(j)=(1-t)*x(j)+t*x(j+1);
            y(j)=(1-t)*y(j)+t*y(j+1);
        end
        
    end

    curveX=x(1);
    curveY=y(1);

end

