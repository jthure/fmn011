function [ x ] = bisection( f,a,b, tolerance )
%BISECTION Summary of this function goes here
%   Detailed explanation goes here

while((b-a)/2>tolerance)
    c=(b+a)/2;
    if(f(c)*f(a)>0)
        a=c;
    elseif(f(c)*f(b)>0)
        b=c;
    else break
    end
end
x=(b+a)/2;

end

