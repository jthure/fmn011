function [ x ] = solve_fixed_point( f, x0, tolerance )
%SOLVE_FIXED_POINT Solves the equation x = f(x) using fixed point method
%   x0 is the initial guess and tolerance is the accuracy.
    xold=x0;    
    x = f(x0);
    
    while abs(xold-x)>tolerance
        xold=x;
        x=f(x);
        
    end
end
