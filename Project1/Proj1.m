%% Find approximate k, with bisection method
f=@(k)(10+1./(k.*2)).*exp(-k)-(7+1./(k.*2));

% Plot f to find a and b such that f(a)*f(b)<0
k_array=linspace(0,1);
plot(k_array,zeros(length(k_array)),k_array,f(k_array))
grid
legend('y1=0','y2=f(k)');
xlabel('k')
ylabel('y')

% Solve for k with an accuracy of 2 decimal places
tolerance=10^(-2);
a=0.2; b=0.4;
num_iter=ceil(log2((b-a)/tolerance))
[k0,err,residual]=solve_bisection(f,a,b,tolerance,num_iter)
k0

%% Find k with fixed point method, using previous k as guess

%Two different fixed point functions k=g(k)
g1=@(k) log((10+1./(2.*k))./(7+1./(2.*k)));
g2=@(k) (10+1./(2.*k)).*exp(-k)-7-1./(2.*k)+k;

% Plot functions to determine slope of gi(k) at intersection with y=k
k_array=linspace(0,1);
plot(k_array,k_array,k_array,g1(k_array),k_array,g2(k_array))
grid
axis([0 1 0 1]);
legend('y1=k','y2=g1(k)','y3=g2(k)');
xlabel('k')
ylabel('y')

%Find k with an accuracy of 6 decimal places using g1(k)
tolerance=10^(-6);
k=solve_fixed_point(g1,k0,tolerance)

%%  Find time of death, td, with Newton method

% Define h(t) and h'(t)
h=@(t) -15+0.5.*t-1./(2*k)+(10+1./(2*k)).*exp(-k.*t);
h_prime=@(t) 0.5-k*(10+1/(2*k)).*exp(-k.*t);

% Plot the function to find suitable t0
t_array=linspace(-2,0);
plot(t_array,zeros(length(t_array)),t_array,h(t_array))
grid
legend('y1=0','y2=h(t)');
xlabel('t')
ylabel('y')

% Run algorithm
t0=-1.4;
[td,iter]=solve_newton(h,h_prime,t0)

%% Find time of death, td, using bisection method

% Start by plotting the function to find suitable a and b
t_array=linspace(-2,0);
plot(t_array,zeros(length(t_array)),t_array,h(t_array))
grid
legend('y1=0','y2=h(t)');
xlabel('t')
ylabel('y')

% Run algorithm
a=-1.4;b=-1.2;
[ result, possible_error, residual, iterations]=solve_bisection_2(h,a,b)

%% Find time of death, td, using fixed point method

% Different functions t=g(t)
g3 = @(t) 30 + 1 / k - (20 + 1 / k) .*exp(-k.*t);
g4 = @(t) log((15+1/(2*k)-0.5*t)/(10+1/(2*k)))/(-k);

% Plot functions to determine slope of gi(k) at intersection with y=k
t_array=linspace(-2,0);
plot(t_array,t_array,t_array,g3(t_array),t_array,g4(t_array));
legend('y1=t','y2=g3(t)','y3=g4(t)');
xlabel('t')
ylabel('y')
axis([-2 -1 -2 -1])

% Run algorithm
t0=-1.4;
[td,iterations]=solve_fixed_point_2(g4, t0)
