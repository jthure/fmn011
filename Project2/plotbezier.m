function   plotbezier( bx,by )
%PLOTBEZIER Summary of this function goes here
%   Detailed explanation goes here
t=linspace(0,1);

plot(bx,by,'r:');
hold on

fx=zeros(1,length(t));
fy=zeros(1,length(t));
for i=1:length(t)
    [fx(i), fy(i)]=casteljau(bx,by,t(i));
end

plot(bx,by,'rs')
plot(fx,fy)
hold off
scalemax=max(max(bx),max(by));
scalemin=min(min(bx),min(by));
axis([scalemin scalemax scalemin scalemax])
%axis([0 612 0 792])

end

