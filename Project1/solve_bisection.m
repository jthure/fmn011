function [ result, possible_error, residual ] = solve_bisection( f,a,b,tolerance,max_iterations )
%SOLVE_BISECTION Solves the function f(x) = 0 with bisection method. 
%   a and b are starting points and are assumed to satisfy f(a)*f(b)<0. Furthermore
%   f is assumed to be continuous. Tolerance is the limit for which if the
%   error is lower the function will return. Max_iterations is the max
%   number of iterations before function returns

    for i=1:max_iterations
        
        c = (a+b)/2;
        if (b-a)/2<tolerance
            break
        end
        
        if f(c)*f(a)>0
            a=c;
        elseif f(c)*f(b)>0
            b=c;
        else break
        end
        
    end
    possible_error=(b-a)/2;
    residual=f(c);
    result = c;
end

